##Team Glomy - Ansible Playbook
* Requires Ansible 2.6

The purpose of this playbook is to install a "toolkit" which contains everything a new start Dev at Cloudreach could possibly need to hit the ground running.
The playbook is compiled using [Ansible](https://www.ansible.com), an open source software that automates software provisioning, config management and application deployment.


###How to...
Clone this repo:
```
https://bitbucket.org/glenelg96/glomy.git
```

Once cloned, 'cd' into the repo and run:
```
ansible-playbook main.yml
```

From there Ansible will work its **magic** and check for Homebrew, install/update as required and then install the following tools to make life a bit easier...

| Brew Cask       | Brew Packages  | Pip Packages   |
| :-------------: |:--------------:|:--------------:|
| Atom            | Docker Compose | AWS-CLI        |
| Docker          | Git            | Pytest         |
| Google Drive    | Packer         | Pylint         |
| Google Hangouts | Pyenv          |                |
| iTerm2          | Python         |                |
| Lastpass        | Terraform      |                |
| Slack           | Tree           |                |
| Intelli-J       | Zsh + Syntax Highlighting       |

##File Structure
```
├── README.md
└── brew
    ├── ansible.cfg
    ├── group_vars
    │   └── all.yml
    ├── inventory.hosts
    └── main.yml
```
##ToDo
* Consider additional useful installs
* Google Chrome bookmarks for Cloudreach Wiki, Cloudy Uni, ACG etc
* Refactor using blocks

##Licence
Cloudreach Europe Ltd. 2018